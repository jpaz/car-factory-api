# Car Factory API

To run the api locally you can use the following command

```
./mvnw spring-boot:run
```

* for more details visit the official <a href="https://car-factory-api.herokuapp.com/" target="_blank">API Documentation</a>
