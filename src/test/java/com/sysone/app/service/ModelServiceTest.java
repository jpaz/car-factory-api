package com.sysone.app.service;

import com.sysone.app.exceptions.ResourceNotFoundException;
import com.sysone.app.model.Model;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ModelServiceTest {

    private final IModelsService modelsService;

    @Autowired
    public ModelServiceTest(IModelsService modelsService) {
        this.modelsService = modelsService;
    }

    @Test
    public void saveAndRetrieveModel() {
        Model model = new Model();
        model.setName("SAVE AND RETIEVE MODEL");
        model.setPrice(100000000D);

        modelsService.save(model);

        Model found = modelsService.findById(model.getId());

        assertNotNull(found);
        assertEquals(model.getName(), found.getName());
        assertEquals(model.getPrice(), found.getPrice());
    }

    @Test
    public void saveAndCountIncrement() {
        Model model = new Model();
        model.setName("SAVE AND COUNT INCREMENT");
        model.setPrice(100000000D);

        Long prev = modelsService.count();

        modelsService.save(model);

        Long count = modelsService.count();

        assertEquals(prev + 1, count);
    }

    @Test
    public void deleteAndRetrieveModelThrowException() {
        Model model = new Model();
        model.setName("DELETE AND RETRIEVE MODEL");
        model.setPrice(200000000D);

        modelsService.save(model);

        int id = model.getId();

        modelsService.delete(model);

        assertThrows(ResourceNotFoundException.class, () -> modelsService.findById(id));
    }

    @Test
    public void deleteByIdAndRetrieveModelThrowException() {
        Model model = new Model();
        model.setName("DELETE BY ID AND RETRIEVE MODEL");
        model.setPrice(200000000D);

        modelsService.save(model);

        int id = model.getId();

        modelsService.deleteById(id);

        assertThrows(ResourceNotFoundException.class, () -> modelsService.findById(id));
    }

    @Test
    public void deletedElementThatDoesNotExistAndThrowsException() {
        Model model = new Model();
        model.setName("DELETE BY ID AND RETRIEVE MODEL");
        model.setPrice(200000000D);

        modelsService.save(model);

        int id = model.getId();

        modelsService.deleteById(id);

        assertThrows(ResourceNotFoundException.class, () -> modelsService.deleteById(id));
    }

    @Test
    public void saveModelsAndRetrieveAll() {
        List<Integer> ids = new LinkedList<>();
        List<String> names = new LinkedList<>();
        List<Double> prices = new LinkedList<>();

        Model model = new Model();
        model.setName("RETRIEVE ALL 001");
        model.setPrice(100000000D);

        modelsService.save(model);
        ids.add(model.getId());
        names.add(model.getName());
        prices.add(model.getPrice());

        model = new Model();
        model.setName("RETRIEVE ALL 002");
        model.setPrice(200000000D);

        modelsService.save(model);
        ids.add(model.getId());
        names.add(model.getName());
        prices.add(model.getPrice());

        List<Model> models = modelsService.findAll();
        List<Integer> foundIds = models.stream().map(Model::getId).collect(Collectors.toList());
        List<String> foundNames = models.stream().map(Model::getName).collect(Collectors.toList());
        List<Double> foundPrices = models.stream().map(Model::getPrice).collect(Collectors.toList());

        assertTrue(foundIds.containsAll(ids));
        assertTrue(foundNames.containsAll(names));
        assertTrue(foundPrices.containsAll(prices));
    }
}