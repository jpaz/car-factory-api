package com.sysone.app.service;

import com.sysone.app.exceptions.ResourceNotFoundException;
import com.sysone.app.model.Equipment;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class EquipmentServiceTest {

    private final IEquipmentsService equipmentsService;

    @Autowired
    public EquipmentServiceTest(IEquipmentsService equipmentsService) {
        this.equipmentsService = equipmentsService;
    }

    @Test
    public void saveAndRetrieveEquipment() {
        Equipment equipment = new Equipment();
        equipment.setName("SAVE AND RETRIEVE EQUIPMENT");
        equipment.setPrice(1000000D);

        equipmentsService.save(equipment);

        Equipment found = equipmentsService.findById(equipment.getId());

        assertNotNull(found);
        assertEquals(equipment.getName(), found.getName());
        assertEquals(equipment.getPrice(), found.getPrice());
    }

    @Test
    public void saveAndCountIncrement() {
        Equipment equipment = new Equipment();
        equipment.setName("SAVE AND COUNT INCREMENT");
        equipment.setPrice(100000000D);

        Long prev = equipmentsService.count();

        equipmentsService.save(equipment);

        Long count = equipmentsService.count();

        assertEquals(prev + 1, count);
    }

    @Test
    public void deleteAndRetrieveEquipmentThrowException() {
        Equipment equipment = new Equipment();
        equipment.setName("DELETE AND RETRIEVE EQUIPMENT");
        equipment.setPrice(200000000D);

        equipmentsService.save(equipment);

        int id = equipment.getId();

        equipmentsService.delete(equipment);

        assertThrows(ResourceNotFoundException.class, () -> equipmentsService.findById(id));
    }

    @Test
    public void deleteByIdAndRetrieveEquipmentThrowException() {
        Equipment equipment = new Equipment();
        equipment.setName("DELETE BY ID AND RETRIEVE EQUIPMENT");
        equipment.setPrice(200000000D);

        equipmentsService.save(equipment);

        int id = equipment.getId();

        equipmentsService.deleteById(id);

        assertThrows(ResourceNotFoundException.class, () -> equipmentsService.findById(id));
    }

    @Test
    public void deletedElementThatDoesNotExistAndThrowsException() {
        Equipment equipment = new Equipment();
        equipment.setName("DELETE BY ID AND RETRIEVE EQUIPMENT");
        equipment.setPrice(200000000D);

        equipmentsService.save(equipment);

        int id = equipment.getId();

        equipmentsService.deleteById(id);

        assertThrows(ResourceNotFoundException.class, () -> equipmentsService.deleteById(id));
    }

    @Test
    public void saveEquipmentsAndRetrieveAll() {
        List<Integer> ids = new LinkedList<>();
        List<String> names = new LinkedList<>();
        List<Double> prices = new LinkedList<>();

        Equipment equipment = new Equipment();
        equipment.setName("RETRIEVE ALL 001");
        equipment.setPrice(100000000D);

        equipmentsService.save(equipment);
        ids.add(equipment.getId());
        names.add(equipment.getName());
        prices.add(equipment.getPrice());

        equipment = new Equipment();
        equipment.setName("RETRIEVE ALL 002");
        equipment.setPrice(200000000D);

        equipmentsService.save(equipment);
        ids.add(equipment.getId());
        names.add(equipment.getName());
        prices.add(equipment.getPrice());

        List<Equipment> equipments = equipmentsService.findAll();
        List<Integer> foundIds = equipments.stream().map(Equipment::getId).collect(Collectors.toList());
        List<String> foundNames = equipments.stream().map(Equipment::getName).collect(Collectors.toList());
        List<Double> foundPrices = equipments.stream().map(Equipment::getPrice).collect(Collectors.toList());

        assertTrue(foundIds.containsAll(ids));
        assertTrue(foundNames.containsAll(names));
        assertTrue(foundPrices.containsAll(prices));
    }
}
