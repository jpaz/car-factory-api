package com.sysone.app.service;

import com.sysone.app.model.Car;
import com.sysone.app.model.Model;
import com.sysone.app.service.implementations.CarsStatsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CarsStatsServiceTest {

    private final ICarsStatsService carsStatsService;
    private final IModelsService modelsService;
    private final ICarsService carsService;

    @Autowired
    public CarsStatsServiceTest(ICarsStatsService carsStatsService, IModelsService modelsService, ICarsService carsService) {
        this.carsStatsService = carsStatsService;
        this.modelsService = modelsService;
        this.carsService = carsService;
    }

    @Test
    public void retrieveStatistics() {
        Map<String, Long> stats = carsStatsService.findAll();

        assertNotNull(stats);
        assertTrue(stats.containsKey("count_car"));
    }

    @Test
    public void retrieveStatsWithoutCars() {
        carsService.findAll().forEach(carsService::delete);
        Map<String, Long> stats = carsStatsService.findAll();

        assertNotNull(stats);
        assertTrue(stats.containsKey("count_car"));
        assertEquals(0, stats.get("count_car"));
    }

    @Test
    public void retrieveStatisticsWithAnUnusedModel() {
        Model model = new Model();
        model.setName("UNUSED MODEL - STATS TEST");
        model.setPrice(100440D);

        modelsService.save(model);

        String countPrefix = CarsStatsService.getCountPrefix(model.getName());
        String percentPrefix = CarsStatsService.getPercentPrefix(model.getName());

        Map<String, Long> stats = carsStatsService.findAll();

        assertNotNull(stats);
        assertTrue(stats.containsKey(countPrefix));
        assertTrue(stats.containsKey(percentPrefix));
        assertEquals(0, stats.get(countPrefix));
        assertEquals(0, stats.get(percentPrefix));
    }

    @Test
    public void retrieveStatisticsWithAUsedModel() {
        Model model = new Model();
        model.setName("USED MODEL - STATS TEST 2");
        model.setPrice(100440D);

        modelsService.save(model);

        Car car = new Car();
        car.setModel(model);

        carsService.save(car);

        String countPrefix = CarsStatsService.getCountPrefix(model.getName());
        String percentPrefix = CarsStatsService.getPercentPrefix(model.getName());

        Map<String, Long> stats = carsStatsService.findAll();

        assertNotNull(stats);
        assertTrue(stats.containsKey(countPrefix));
        assertTrue(stats.containsKey(percentPrefix));
        assertNotEquals(0, stats.get(countPrefix));
        assertNotEquals(0, stats.get(percentPrefix));
    }
}
