package com.sysone.app.service;

import com.sysone.app.exceptions.ResourceNotFoundException;
import com.sysone.app.model.Car;
import com.sysone.app.model.Equipment;
import com.sysone.app.model.Model;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CarsServiceTest {

    private final ICarsService carsService;
    private final IModelsService modelsService;
    private final IEquipmentsService equipmentsService;

    @Autowired
    public CarsServiceTest(ICarsService cs, IModelsService ms, IEquipmentsService es) {
        this.carsService = cs;
        this.modelsService = ms;
        this.equipmentsService = es;
    }

    @Test
    public void saveAndRetrieveCar() {
        Model model = modelsService.findAll().get(0);
        Set<Equipment> equipments = new HashSet<>(equipmentsService.findAll());
        Car car = new Car();
        car.setModel(model);
        car.setEquipments(equipments);

        carsService.save(car);

        Car found = carsService.findById(car.getId());
        Model foundModel = car.getModel();
        Set<Equipment> foundEquipments = car.getEquipments();
        List<Integer> equipmentsIds = foundEquipments.stream().map(Equipment::getId).collect(Collectors.toList());
        List<String> equipmentsNames = foundEquipments.stream().map(Equipment::getName).collect(Collectors.toList());
        List<Double> equipmentsPrices = foundEquipments.stream().map(Equipment::getPrice).collect(Collectors.toList());

        assertNotNull(found);
        assertEquals(car.getId(), found.getId());
        assertEquals(car.getPrice(), found.getPrice());
        assertEquals(model.getId(), foundModel.getId());
        assertEquals(model.getName(), foundModel.getName());
        assertEquals(model.getPrice(), foundModel.getPrice());
        assertTrue(equipments.stream().allMatch(e ->
                equipmentsIds.contains(e.getId()) &&
                        equipmentsNames.contains(e.getName()) &&
                        equipmentsPrices.contains(e.getPrice())
        ));
    }

    @Test
    public void saveAndCountIncrement() {
        Model model = modelsService.findAll().get(0);
        Car car = new Car();
        car.setModel(model);

        Long prev = carsService.count();

        carsService.save(car);

        Long count = carsService.count();

        assertEquals(prev + 1, count);
    }

    @Test
    public void deleteAndRetrieveCarThrowException() {
        Car car = new Car();
        car.setModel(modelsService.findAll().get(0));

        carsService.save(car);

        int id = car.getId();

        carsService.delete(car);

        assertThrows(ResourceNotFoundException.class, () -> carsService.findById(id));
    }

    @Test
    public void deleteByIdAndRetrieveCarThrowException() {
        Car car = new Car();
        car.setModel(modelsService.findAll().get(0));

        carsService.save(car);

        int id = car.getId();

        carsService.deleteById(id);

        assertThrows(ResourceNotFoundException.class, () -> carsService.findById(id));
    }

    @Test
    public void deletedElementThatDoesNotExistAndThrowsException() {
        Car car = new Car();
        car.setModel(modelsService.findAll().get(0));

        carsService.save(car);

        int id = car.getId();

        carsService.deleteById(id);

        assertThrows(ResourceNotFoundException.class, () -> carsService.deleteById(id));
    }

    @Test
    public void saveCarsAndRetrieveAll() {
        List<Integer> carsIds = new LinkedList<>();
        Model model = modelsService.findAll().get(0);
        Set<Equipment> equipments = new HashSet<>(equipmentsService.findAll());
        List<Integer> equipmentsIds = equipments.stream().map(Equipment::getId).collect(Collectors.toList());

        Car car = new Car();
        car.setModel(model);
        car.setEquipments(equipments);
        carsService.save(car);
        carsIds.add(car.getId());

        car = new Car();
        car.setModel(model);
        car.setEquipments(equipments);
        carsService.save(car);
        carsIds.add(car.getId());

        car = new Car();
        car.setModel(model);
        car.setEquipments(equipments);
        carsService.save(car);
        carsIds.add(car.getId());

        List<Car> cars = carsService.findAll();
        List<Integer> foundIds = cars.stream().map(Car::getId).collect(Collectors.toList());
        List<Integer> foundModelsIds = cars.stream().map(Car::getModel).map(Model::getId).collect(Collectors.toList());
        List<Integer> foundEquipmentsIds = new LinkedList<>();
        cars.stream().map(Car::getEquipments).forEach(e -> foundEquipmentsIds.addAll(e.stream().map(Equipment::getId)
                .collect(Collectors.toList())));

        assertFalse(cars.isEmpty());
        assertTrue(foundIds.containsAll(carsIds));
        assertTrue(foundModelsIds.contains(model.getId()));
        assertTrue(foundEquipmentsIds.containsAll(equipmentsIds));
    }

    @Test
    public void countingByNonExistentModelGivesZero() {
        Model model = new Model();
        model.setName("MODELS TEST");
        model.setPrice(192371D);

        modelsService.save(model);
        int id = model.getId();
        modelsService.deleteById(id);

        assertEquals(0, carsService.countByModelId(id));
    }

    @Test
    public void countingByExistentModelDoesNotGiveZero() {
        Model model = new Model();
        model.setName("MODELS TEST");
        model.setPrice(192371D);

        modelsService.save(model);
        int id = model.getId();

        Car car = new Car();
        car.setModel(model);

        carsService.save(car);

        assertNotEquals(0, carsService.countByModelId(id));
    }

    @Test
    public void countingByNonExistentEquipmentGivesZero() {
        Equipment equipment = new Equipment();
        equipment.setName("EQUIPMENT TEST");
        equipment.setPrice(10418D);

        equipmentsService.save(equipment);
        int id = equipment.getId();
        equipmentsService.deleteById(id);

        assertEquals(0, carsService.countByEquipmentId(id));
    }

    @Test
    public void countingByExistentEquipmentDoesNotGiveZero() {
        Equipment equipment = new Equipment();
        equipment.setName("EQUIPMENT TEST");
        equipment.setPrice(10418D);
        Model model = new Model();
        model.setName("MODEL TEST");
        model.setPrice(123132D);

        equipmentsService.save(equipment);
        modelsService.save(model);
        int id = equipment.getId();

        Car car = new Car();
        car.setModel(model);
        car.addEquipment(equipment);

        carsService.save(car);

        assertNotEquals(0, carsService.countByEquipmentId(id));
    }
}
