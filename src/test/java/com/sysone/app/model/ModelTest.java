package com.sysone.app.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ModelTest {
    @Test
    public void createModel() {
        Model model = new Model();
        assertNotNull(model);
    }

    @Test
    public void createModelAndSetState() {
        int id = 1;
        String name = "Model test";
        double price = 1234D;

        Model model = new Model();
        model.setId(id);
        model.setName(name);
        model.setPrice(price);

        assertNotNull(model);
        assertEquals(id, model.getId());
        assertEquals(name, model.getName());
        assertEquals(price, model.getPrice());
    }

    @Test
    public void toStringRetrieveNotNull() {
        int id = 1;
        String name = "Model test";
        double price = 1234D;

        Model model = new Model();
        model.setId(id);
        model.setName(name);
        model.setPrice(price);

        assertNotNull(model.toString());
    }
}
