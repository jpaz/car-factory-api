package com.sysone.app.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class EquipmentTest {
    @Test
    public void createEquipment() {
        Equipment equipment = new Equipment();
        assertNotNull(equipment);
    }

    @Test
    public void createEquipmentAndSetState() {
        int id = 1;
        String name = "Equipment test";
        double price = 1234D;

        Equipment equipment = new Equipment();
        equipment.setId(id);
        equipment.setName(name);
        equipment.setPrice(price);

        assertNotNull(equipment);
        assertEquals(id, equipment.getId());
        assertEquals(name, equipment.getName());
        assertEquals(price, equipment.getPrice());
    }

    @Test
    public void toStringRetrieveNotNull() {
        int id = 1;
        String name = "Equipment test";
        double price = 1234D;

        Equipment equipment = new Equipment();
        equipment.setId(id);
        equipment.setName(name);
        equipment.setPrice(price);

        assertNotNull(equipment.toString());
    }
}
