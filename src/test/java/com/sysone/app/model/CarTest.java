package com.sysone.app.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CarTest {
    @Test
    public void createCar() {
        Car car = new Car();
        assertNotNull(car);
    }

    @Test
    public void createCarAndSetState() {
        int carID = 100;

        Set<Equipment> equipments = new HashSet<>();

        Model model = new Model();
        model.setId(1);
        model.setName("MODEL TEST - CREATE CAR");
        model.setPrice(10000D);

        Car car = new Car(model);
        car.setId(carID);

        Equipment equipment = new Equipment();
        equipment.setId(1);
        equipment.setName("EQUIPMENT 1 - CREATE CAR");
        equipment.setPrice(10000D);
        equipments.add(equipment);
        car.addEquipment(equipment);

        equipment = new Equipment();
        equipment.setId(2);
        equipment.setName("EQUIPMENT 2 - CREATE CAR");
        equipment.setPrice(20000D);
        equipments.add(equipment);
        car.addEquipment(equipment);

        double carPrice = model.getPrice() + equipments.stream().map(Equipment::getPrice).reduce(0D, Double::sum);
        List<Integer> equipmentsIds = car.getEquipments().stream().map(Equipment::getId).collect(Collectors.toList());
        List<String> equipmentsNames = car.getEquipments().stream().map(Equipment::getName).collect(Collectors.toList());
        List<Double> equipmentsPrices = car.getEquipments().stream().map(Equipment::getPrice).collect(Collectors.toList());

        assertNotNull(car);
        assertEquals(carID, car.getId());
        assertEquals(carPrice, car.getPrice());
        assertEquals(model.getId(), car.getModel().getId());
        assertEquals(model.getName(), car.getModel().getName());
        assertEquals(model.getPrice(), car.getModel().getPrice());
        assertTrue(equipmentsIds.containsAll(equipments.stream().map(Equipment::getId).collect(Collectors.toList())));
        assertTrue(equipmentsNames.containsAll(equipments.stream().map(Equipment::getName).collect(Collectors.toList())));
        assertTrue(equipmentsPrices.containsAll(equipments.stream().map(Equipment::getPrice).collect(Collectors.toList())));
    }

    @Test
    public void toStringRetrieveNotNull() {
        Set<Equipment> equipments = new HashSet<>();

        Model model = new Model();
        model.setId(1);
        model.setName("MODEL TEST - CREATE CAR");
        model.setPrice(10000D);

        Equipment equipment = new Equipment();
        equipment.setId(1);
        equipment.setName("EQUIPMENT 1 - CREATE CAR");
        equipment.setPrice(10000D);
        equipments.add(equipment);

        equipment = new Equipment();
        equipment.setId(2);
        equipment.setName("EQUIPMENT 2 - CREATE CAR");
        equipment.setPrice(20000D);
        equipments.add(equipment);

        Car car = new Car(model, equipments);

        assertNotNull(car.toString());
    }

    @Test
    public void setPriceAndRecalculateTheOriginal() {
        int carID = 100;
        Set<Equipment> equipments = new HashSet<>();

        Model model = new Model();
        model.setId(1);
        model.setName("MODEL TEST - CREATE CAR");
        model.setPrice(10000D);

        Equipment equipment = new Equipment();
        equipment.setId(1);
        equipment.setName("EQUIPMENT 1 - CREATE CAR");
        equipment.setPrice(10000D);
        equipments.add(equipment);

        equipment = new Equipment();
        equipment.setId(2);
        equipment.setName("EQUIPMENT 2 - CREATE CAR");
        equipment.setPrice(20000D);
        equipments.add(equipment);

        Car car = new Car(model, equipments);

        double originalPrice = car.getPrice();

        car.setPrice(originalPrice + 12312312D);

        car.calculatePrice();

        assertEquals(originalPrice, car.getPrice());
    }
}
