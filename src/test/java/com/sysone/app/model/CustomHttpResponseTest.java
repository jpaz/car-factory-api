package com.sysone.app.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CustomHttpResponseTest {
    @Test
    public void createWithEmptyConstructor() {
        CustomHttpResponse response = new CustomHttpResponse();
        assertNotNull(response);
    }

    @Test
    public void createWithNonEmptyConstructor() {
        String message = "ERROR TEST MESSAGE";
        CustomHttpResponse response = new CustomHttpResponse(HttpStatus.OK, message);

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatus());
        assertEquals(message, response.getError());
    }

    @Test
    public void setAndGetState() {
        String message = "ERROR TEST MESSAGE";
        LocalDateTime localDate = LocalDateTime.now();
        HttpStatus status = HttpStatus.OK;

        CustomHttpResponse response = new CustomHttpResponse();
        response.setError(message);
        response.setDate(localDate);
        response.setStatus(status);

        assertNotNull(response);
        assertEquals(message, response.getError());
        assertEquals(localDate, response.getDate());
        assertEquals(HttpStatus.OK, response.getStatus());
    }

    @Test
    public void toStringIsNotNull() {
        String message = "ERROR TEST MESSAGE";
        LocalDateTime localDate = LocalDateTime.now();
        HttpStatus status = HttpStatus.OK;

        CustomHttpResponse response = new CustomHttpResponse();
        response.setError(message);
        response.setDate(localDate);
        response.setStatus(status);

        assertNotNull(response.toString());
    }
}
