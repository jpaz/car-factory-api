package com.sysone.app.testapp;

import com.sysone.app.model.Car;
import com.sysone.app.model.Equipment;
import com.sysone.app.model.Model;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class TestApp {
    public static List<Equipment> opcionales;
    public static List<Model> models;

    static {
        opcionales = new LinkedList<>();
        models = new LinkedList<>();

        // Opcionales para Automoviles

        Equipment equipment = new Equipment();
        equipment.setId(1);
        equipment.setName("Techo corredizo");
        equipment.setPrice(12000d);
        opcionales.add(equipment);

        equipment = new Equipment();
        equipment.setId(2);
        equipment.setName("Aire acondicionado");
        equipment.setPrice(20000d);
        opcionales.add(equipment);

        equipment = new Equipment();
        equipment.setId(3);
        equipment.setName("Sistema de frenos ABS");
        equipment.setPrice(14000d);
        opcionales.add(equipment);

        equipment = new Equipment();
        equipment.setId(4);
        equipment.setName("Airbag");
        equipment.setPrice(7000d);
        opcionales.add(equipment);

        equipment = new Equipment();
        equipment.setId(5);
        equipment.setName("Llantas de aleacion");
        equipment.setPrice(12000d);
        opcionales.add(equipment);

        // Modelos de Automovil

        Model model = new Model();
        model.setId(1);
        model.setName("SEDAN");
        model.setPrice(230000d);
        models.add(model);

        model = new Model();
        model.setId(2);
        model.setName("FAMILIAR");
        model.setPrice(245000d);
        models.add(model);

        model = new Model();
        model.setId(3);
        model.setName("COUPE");
        model.setPrice(270000d);
        models.add(model);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Car car = new Car();
        Model model;
        int opcion;

        do {
            System.out.println("Seleccione el modelo de Automovil: ");
            models.forEach(TestApp::printModelo);
            opcion = scanner.nextInt();
        } while (!isOpcionValida(opcion, models.size()));

        model = models.get(opcion - 1);
        car.setModel(model);
        System.out.println("Modelo elegido: " + model.getName());

        do {
            System.out.println("Seleccione si desea algun opcional: ");
            System.out.println("0) Continuar");
            opcionales.forEach(TestApp::printOpcional);
            opcion = scanner.nextInt();

            if (isOpcionValida(opcion, opcionales.size()) && opcion != 0) {
                Equipment equipment = opcionales.get(opcion - 1);
                car.addEquipment(equipment);
                System.out.println("Opcional elegido: " + equipment.getName());
            }

        } while (opcion != 0);

        System.out.println("Automovil construido: " + car);
    }

    private static void printModelo(Model model) {
        System.out.println(model.getId() + ") " + model.getName() + ": " + model.getPrice());
    }

    private static void printOpcional(Equipment equipment) {
        System.out.println(equipment.getId() + ") " + equipment.getName() + ": " + equipment.getPrice());
    }

    private static boolean isOpcionValida(int opc, int max) {
        return opc >= 1 && opc <= max;
    }
}
