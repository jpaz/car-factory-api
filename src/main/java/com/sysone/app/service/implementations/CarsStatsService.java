package com.sysone.app.service.implementations;

import com.sysone.app.model.Equipment;
import com.sysone.app.model.Model;
import com.sysone.app.service.ICarsService;
import com.sysone.app.service.ICarsStatsService;
import com.sysone.app.service.IEquipmentsService;
import com.sysone.app.service.IModelsService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CarsStatsService implements ICarsStatsService {

    private final ICarsService carsService;
    private final IEquipmentsService equipmentsService;
    private final IModelsService modelsService;

    public CarsStatsService(ICarsService carsService, IModelsService modelsService, IEquipmentsService equipmentsService) {
        this.carsService = carsService;
        this.modelsService = modelsService;
        this.equipmentsService = equipmentsService;
    }

    @Override
    public Map<String, Long> findAll() {
        Map<String, Long> map = new LinkedHashMap<>();
        Long total = this.carsService.count();

        map.put("count_car", carsService.count());

        List<Model> models = modelsService.findAll();
        models.forEach(
                m -> this.putStats(
                        map, m.getName(),
                        carsService.countByModelId(m.getId()), total
                )
        );

        List<Equipment> equipements = equipmentsService.findAll();
        equipements.forEach(
                o -> this.putStats(
                        map, o.getName(),
                        carsService.countByEquipmentId(o.getId()), total
                )
        );

        return map;
    }

    public static String getInitials(String str) {
        return Arrays.stream(str.split(" "))
                .map(s -> s.substring(0, 1))
                .collect(Collectors.joining())
                .toLowerCase()
                .replace(" ", "_");
    }

    public static String getCountPrefix(String str) {
        return "count_" + getInitials(str);
    }

    public static String getPercentPrefix(String str) {
        return "percent_" + getInitials(str);
    }

    private Long calculatePercent(Long count, Long total) {
        return (count * 100) / total;
    }

    private void putStats(Map<String, Long> map, String name, Long count, Long total) {
        Long percent = total == 0 ? 0 : this.calculatePercent(count, total);
        map.put(getCountPrefix(name), count);
        map.put(getPercentPrefix(name), percent);
    }
}
