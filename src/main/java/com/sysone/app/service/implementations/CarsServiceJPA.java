package com.sysone.app.service.implementations;

import com.sysone.app.exceptions.ResourceNotFoundException;
import com.sysone.app.model.Car;
import com.sysone.app.model.Equipment;
import com.sysone.app.model.Model;
import com.sysone.app.repository.CarsJPARepository;
import com.sysone.app.service.ICarsService;
import com.sysone.app.service.IEquipmentsService;
import com.sysone.app.service.IModelsService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CarsServiceJPA implements ICarsService {

    private final CarsJPARepository carsJPARepository;
    private final IModelsService modelsService;
    private final IEquipmentsService equipmentsService;

    public CarsServiceJPA(CarsJPARepository cr, IModelsService ms, IEquipmentsService es) {
        this.carsJPARepository = cr;
        this.modelsService = ms;
        this.equipmentsService = es;
    }

    @Override
    public List<Car> findAll() {
        return carsJPARepository.findAll();
    }

    @Override
    public Car findById(int id) {
        return carsJPARepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("car with id " + id + " not found")
        );
    }

    @Override
    public Car save(Car car) {
        return carsJPARepository.save(this.fetchAutomovilData(car));
    }

    @Override
    public void delete(Car car) {
        this.deleteById(car.getId());
    }

    @Override
    public void deleteById(int id) {
        if (!carsJPARepository.existsById(id)) {
            throw new ResourceNotFoundException("car with id " + id + " not found");
        }

        carsJPARepository.deleteById(id);
    }

    @Override
    public Long count() {
        return carsJPARepository.count();
    }

    @Override
    public Long countByModelId(int id) {
        return carsJPARepository.countByModel_Id(id);
    }

    @Override
    public Long countByEquipmentId(int id) {
        return carsJPARepository.countByEquipment_Id(id);
    }

    private Car fetchAutomovilData(Car car) {
        Model model = modelsService.findById(car.getModel().getId());
        Set<Equipment> equipments = car.getEquipments().stream()
                .map(o -> equipmentsService.findById(o.getId()))
                .collect(Collectors.toSet());
        car.setModel(model);
        car.setEquipments(equipments);
        return car;
    }
}
