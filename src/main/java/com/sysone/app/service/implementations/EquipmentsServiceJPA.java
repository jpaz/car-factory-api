package com.sysone.app.service.implementations;

import com.sysone.app.exceptions.ResourceNotFoundException;
import com.sysone.app.model.Equipment;
import com.sysone.app.repository.EquipmentsJPARepository;
import com.sysone.app.service.IEquipmentsService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EquipmentsServiceJPA implements IEquipmentsService {

    private final EquipmentsJPARepository equipmentsJPARepository;

    public EquipmentsServiceJPA(EquipmentsJPARepository equipmentsJPARepository) {
        this.equipmentsJPARepository = equipmentsJPARepository;
    }

    @Override
    public List<Equipment> findAll() {
        return equipmentsJPARepository.findAll();
    }

    @Override
    public Equipment findById(int id) {
        return equipmentsJPARepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("equipment with id " + id + " not found")
        );
    }

    @Override
    public Equipment save(Equipment equipment) {
        return equipmentsJPARepository.save(equipment);
    }

    @Override
    public void delete(Equipment equipment) {
        this.deleteById(equipment.getId());
    }

    @Override
    public void deleteById(int id) {
        if (!equipmentsJPARepository.existsById(id)) {
            throw new ResourceNotFoundException("equipment with id " + id + " not found");
        }

        equipmentsJPARepository.deleteById(id);
    }

    @Override
    public Long count() {
        return equipmentsJPARepository.count();
    }
}
