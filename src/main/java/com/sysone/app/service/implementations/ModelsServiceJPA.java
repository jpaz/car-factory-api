package com.sysone.app.service.implementations;

import com.sysone.app.exceptions.ResourceNotFoundException;
import com.sysone.app.model.Model;
import com.sysone.app.repository.ModelsJPARepository;
import com.sysone.app.service.IModelsService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModelsServiceJPA implements IModelsService {

    private final ModelsJPARepository modeloRepository;

    public ModelsServiceJPA(ModelsJPARepository modeloRepository) {
        this.modeloRepository = modeloRepository;
    }

    @Override
    public List<Model> findAll() {
        return modeloRepository.findAll();
    }

    @Override
    public Model findById(int id) {
        return modeloRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("model with id " + id + " not found")
        );
    }

    @Override
    public Model save(Model model) {
        return modeloRepository.save(model);
    }

    @Override
    public void delete(Model model) {
        this.deleteById(model.getId());
    }

    @Override
    public void deleteById(int id) {
        if (!modeloRepository.existsById(id)) {
            throw new ResourceNotFoundException("model with id " + id + " not found");
        }

        modeloRepository.deleteById(id);
    }

    @Override
    public Long count() {
        return modeloRepository.count();
    }
}
