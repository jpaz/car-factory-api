package com.sysone.app.service;

import com.sysone.app.model.Equipment;

public interface IEquipmentsService extends IEntityCRUDService<Equipment> {
}
