package com.sysone.app.service;

import java.util.Map;

public interface ICarsStatsService {
    Map<String, Long> findAll();
}
