package com.sysone.app.service;

import com.sysone.app.model.Model;

public interface IModelsService extends IEntityCRUDService<Model> {
}
