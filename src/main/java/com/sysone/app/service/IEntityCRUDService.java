package com.sysone.app.service;

import com.sysone.app.exceptions.ResourceNotFoundException;

import java.util.List;

public interface IEntityCRUDService <E> {
    List<E> findAll();

    E findById(int id) throws ResourceNotFoundException;

    E save(E entity);

    void delete(E entity);

    void deleteById(int id);

    Long count();
}
