package com.sysone.app.service;

import com.sysone.app.model.Car;

public interface ICarsService extends IEntityCRUDService<Car> {
    Long countByModelId(int id);

    Long countByEquipmentId(int id);
}
