package com.sysone.app.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "cars")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private double price;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_model", nullable = false)
    private Model model;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "cars_equipments",
            joinColumns = @JoinColumn(name = "id_car"),
            inverseJoinColumns = @JoinColumn(name = "id_equipment")
    )
    private Set<Equipment> equipments;

    public Car() {
        this.equipments = new HashSet<>();
    }

    public Car(Model model) {
        this();
        this.model = model;
        this.price = this.model.getPrice();
    }

    public Car(Model model, Set<Equipment> equipments) {
        this.model = model;
        this.price = this.model.getPrice();
        this.equipments = equipments;
        this.calculatePrice();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double precio) {
        this.price = precio;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
        this.calculatePrice();
    }

    public Set<Equipment> getEquipments() {
        return equipments;
    }

    public void setEquipments(Set<Equipment> opcionales) {
        this.equipments = opcionales;
        this.calculatePrice();
    }

    public void addEquipment(Equipment equipment) {
        this.equipments.add(equipment);
        this.calculatePrice();
    }

    public void calculatePrice() {
        this.price = this.model.getPrice() + this.equipments.stream()
                .map(Equipment::getPrice)
                .reduce(0d, Double::sum);
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", price=" + price +
                ", model=" + model +
                ", equipments=" + equipments +
                '}';
    }
}
