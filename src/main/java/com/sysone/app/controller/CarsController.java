package com.sysone.app.controller;

import com.sysone.app.model.Car;
import com.sysone.app.service.ICarsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cars")
public class CarsController {

    private final ICarsService carsService;

    public CarsController(ICarsService as) {
        this.carsService = as;
    }

    @GetMapping
    public List<Car> findAll() {
        return carsService.findAll();
    }

    @GetMapping("/{id}")
    public Car findById(@PathVariable("id") int id) {
        return carsService.findById(id);
    }

    @PostMapping
    public ResponseEntity<Car> save(@RequestBody Car car) {
        return new ResponseEntity<>(carsService.save(car), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public Car update(@PathVariable("id") int id, @RequestBody Car car) {
        car.setId(id);
        return carsService.save(car);
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") int id) {
        carsService.deleteById(id);
        return "OK";
    }
}