package com.sysone.app.controller;

import com.sysone.app.model.Equipment;
import com.sysone.app.service.IEquipmentsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/equipments")
public class EquipmentsController {

    private final IEquipmentsService equipmentsService;

    public EquipmentsController(IEquipmentsService equipmentsService) {
        this.equipmentsService = equipmentsService;
    }

    @GetMapping
    public List<Equipment> findAll() {
        return equipmentsService.findAll();
    }

    @GetMapping("/{id}")
    public Equipment findById(@PathVariable("id") int id) {
        return equipmentsService.findById(id);
    }

    @PostMapping
    public ResponseEntity<Equipment> save(@RequestBody Equipment equipment) {
        return new ResponseEntity<>(equipmentsService.save(equipment), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public Equipment update(@PathVariable("id") int id, @RequestBody Equipment equipment) {
        equipment.setId(id);
        return equipmentsService.save(equipment);
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") int id) {
        equipmentsService.deleteById(id);
        return "OK";
    }
}