package com.sysone.app.controller;

import com.sysone.app.service.ICarsStatsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/stats")
public class StatsController {

    private final ICarsStatsService carsStatsService;

    public StatsController(ICarsStatsService carsStatsService) {
        this.carsStatsService = carsStatsService;
    }

    @GetMapping
    public Map<String, Long> stats() {
        return carsStatsService.findAll();
    }
}
