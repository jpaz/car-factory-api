package com.sysone.app.controller;

import com.sysone.app.model.Model;
import com.sysone.app.service.IModelsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/models")
public class ModelsController {

    private final IModelsService modelsService;

    public ModelsController(IModelsService modelsService) {
        this.modelsService = modelsService;
    }

    @GetMapping
    public List<Model> findAll() {
        return modelsService.findAll();
    }

    @GetMapping("/{id}")
    public Model findById(@PathVariable("id") int id) {
        return modelsService.findById(id);
    }

    @PostMapping
    public ResponseEntity<Model> save(@RequestBody Model model) {
        return new ResponseEntity<>(modelsService.save(model), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public Model update(@PathVariable("id") int id, @RequestBody Model model) {
        model.setId(id);
        return modelsService.save(model);
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") int id) {
        modelsService.deleteById(id);
        return "OK";
    }
}
