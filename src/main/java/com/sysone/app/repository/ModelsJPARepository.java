package com.sysone.app.repository;

import com.sysone.app.model.Model;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModelsJPARepository extends JpaRepository<Model, Integer> {
}
