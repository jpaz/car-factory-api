package com.sysone.app.repository;

import com.sysone.app.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CarsJPARepository extends JpaRepository<Car, Integer> {
    Long countByModel_Id(int id);

    @Query(value="SELECT COUNT(*) FROM cars_equipments WHERE cars_equipments.id_equipment=:id", nativeQuery=true)
    Long countByEquipment_Id(@Param("id") int id);
}
