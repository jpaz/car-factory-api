const JSON_FORMAT_REGEX = /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g;

const normalizeJSON = json => {
    return json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

const getJSONSpanByType = match => {
    let cls = 'number';
    if (/^"/.test(match)) {
        if (/:$/.test(match)) {
            cls = 'key';
        } else {
            cls = 'string';
        }
    } else if (/true|false/.test(match)) {
        cls = 'boolean';
    } else if (/null/.test(match)) {
        cls = 'null';
    }
    return '<span class="' + cls + '">' + match + '</span>';
}

const syntaxHighlight = json => {
    return normalizeJSON(json).replace(JSON_FORMAT_REGEX, getJSONSpanByType);
}

const prettyJsons = () => {
    const terminals = document.querySelectorAll(".terminal");
    for (const terminal of terminals) {
        const arr = terminal.querySelectorAll("pre");
        if (arr) {
            for (const element of arr) {
                const object = JSON.parse(element.innerText);
                const str = JSON.stringify(object, undefined, 2);
                element.innerHTML = syntaxHighlight(str);
            }
        }
    }
}

const getPaginateItems = () => {
    return document.querySelector('#pagination').querySelectorAll('.paginate-item');
}

const getPaginateTargets = () => {
    return document.querySelector('#content').querySelectorAll('.paginate-target');
}

const getCollapseMenuByListItem = listItem => {
    return listItem.parentElement;
}

const getCollapseByCollapseControl = (collapseMenus, collapseControl) => {
    for (const collapseMenu of collapseMenus) {
        if (`#${collapseMenu.getAttribute('id')}` === collapseControl.getAttribute('href')) {
            return collapseMenu;
        }
    }
    return null;
}

const getCollapseControlsData = () => {
    const collapseControls = document.querySelector('#pagination').querySelectorAll('.collapse-control');
    const collapseMenus = document.querySelector('#pagination').querySelectorAll('.collapse');
    const result = [];
    collapseControls.forEach(collapseControl => {
        result.push({
            control: collapseControl,
            collapse: getCollapseByCollapseControl(collapseMenus, collapseControl)
        });
    })
    return result;
}

const getPaginateTargetByTargetId = (paginateTargets, targetId) => {
    for (const paginateTarget of paginateTargets) {
        if (`#${paginateTarget.id}` === targetId) {
            return paginateTarget;
        }
    }
    return null;
}

const getPaginateListItemsAndTargetsAndCollapseManu = () => {
    const paginateItems = getPaginateItems();
    const paginateTargets = getPaginateTargets();
    const result = [];
    paginateItems.forEach(p => {
        result.push({
            item: p,
            target: getPaginateTargetByTargetId(paginateTargets, p.getAttribute('href')),
            collapse: getCollapseMenuByListItem(p)
        });
    })
    return result;
}

const setDropdownItemActions = () => {
    const dropDownItems = document.querySelector('#mobile-navbar').querySelectorAll('.dropdown-item');
    const collapseNavbar = document.querySelector('#navbarNavDropdown');
    dropDownItems.forEach(dI => {
        dI.addEventListener('click', () => {
            $(collapseNavbar).collapse('hide');
        })
    })
}

const debounce = (fn, time) => {
    let timeout;

    return function () {
        const functionCall = () => fn.apply(this, arguments);

        clearTimeout(timeout);
        timeout = setTimeout(functionCall, time);
    }
}

const manageCollapseMenus = () => {
    let collapseActive = null;
    let itemActive = null;
    let debounceTime = 200;

    const setCollapseActive = debounce(collapse => {
        if (collapse && collapseActive !== collapse) {
            $(collapseActive).collapse('hide');
            $(collapse).collapse('show');
            collapseActive = collapse;
        }
    }, debounceTime);

    const setItemActive = debounce(item => {
        if (item && itemActive !== item) {
            if (itemActive) {
                itemActive.classList.toggle('active');
            }
            item.classList.toggle('active');
            itemActive = item;
        }
    }, debounceTime / 4);

    const manageCollapseControls = collapseDataControls => {
        collapseDataControls.forEach(({control, collapse}) => {
            control.addEventListener('click', debounce(() => {
                // noinspection JSCheckFunctionSignatures
                setCollapseActive(collapse);
            }, debounceTime / 2));
        });
    }

    const updateCollapseMenus = debounce(paginateItemsData => {
        paginateItemsData.forEach(({item, target, collapse}) => {
            if (target) {
                const position = target.getBoundingClientRect();
                if (position.top < window.screen.height / 2) {
                    // noinspection JSCheckFunctionSignatures
                    setCollapseActive(collapse);
                    // noinspection JSCheckFunctionSignatures
                    setItemActive(item);
                }
            }
        });
    }, debounceTime);

    const collapseDataControls = getCollapseControlsData();
    const paginateData = getPaginateListItemsAndTargetsAndCollapseManu();

    manageCollapseControls(collapseDataControls);
    // noinspection JSCheckFunctionSignatures
    updateCollapseMenus(paginateData);
    // noinspection JSCheckFunctionSignatures
    setDropdownItemActions();
    document.querySelector('#content').addEventListener('scroll', () => updateCollapseMenus(paginateData));
}

document.addEventListener('DOMContentLoaded', () => {
    prettyJsons();
    manageCollapseMenus();
});